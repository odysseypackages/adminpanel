<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logout' => 'Wyloguj',
    'profile' => 'Profil',
    'save' => 'Zapisz',
    'menu' => [
      'start' => 'Start',
      'config' => 'Konfigurcja'
    ],
    'lfm' => [
        'choose' => 'Wybierz pliki'
    ],
    'dashboard' => [
        'title' => 'Pulpit',
        'subtitle' => 'Strona główna panelu administracyjnego'
    ],
    'config' => [
        'title' => 'Konfiguracja',
        'subtitle' => 'Edytuj ustawienia systemu',
        'alert' => [
            'success' => 'Konfiguracja zapisana pomyślnie'
        ]
    ]
];
