<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE"/>
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#75C7C3">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#75C7C3">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#75C7C3">

    <title>{{ trans('laravel-filemanager::lfm.title-page') }}</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('vendor/laravel-filemanager/img/folder.png') }}">
    <link rel="stylesheet" href="{{ asset('vendor/admin-panel/css/adminPanel.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/cropper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/laravel-filemanager/css/lfm.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/mfb.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/laravel-filemanager/css/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">
</head>
<body class="lfm-body">
<div class="container-fluid" id="wrapper">
    <div class="panel panel-primary hidden-xs">
        <div class="panel-heading">
            <h1 class="panel-title">{{ trans('laravel-filemanager::lfm.title-panel') }}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2 hidden-xs">
            <div id="tree"></div>
        </div>

        <div class="col-sm-10 col-xs-12" id="main">
            <nav class="navbar navbar-default nav" id="nav">
                <ul class="nav nav-pills">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle fa fa-bars" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false"></a>
                        <div class="dropdown-menu" x-placement="bottom-start">
                            <a class="dropdown-item" href="#" id="thumbnail-display">
                                <i class="fa fa-th-large"></i>
                                <span>{{ trans('laravel-filemanager::lfm.nav-thumbnails') }}</span>
                            </a>
                            <a class="dropdown-item" href="#" id="list-display">
                                <i class="fa fa-list"></i>
                                <span>{{ trans('laravel-filemanager::lfm.nav-list') }}</span>
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle fa fa-sort" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false"></a>
                        <div class="dropdown-menu" x-placement="bottom-start">
                            <a class="dropdown-item" href="#" id="list-sort-alphabetic">
                                <i class="fa fa-sort-alpha-down"></i>
                                {{ trans('laravel-filemanager::lfm.nav-sort-alphabetic') }}
                            </a>
                            <a class="dropdown-item" href="#" id="list-sort-time">
                                <i class="fa fa-sort-amount-down"></i>
                                {{ trans('laravel-filemanager::lfm.nav-sort-time') }}
                            </a>
                        </div>
                    </li>
                </ul>
                <div class="nav-pills">
                    <a class="hidden clickable" id="to-previous">
                        <i class="fa fa-arrow-left"></i>
                        <span class="hidden-xs">{{ trans('laravel-filemanager::lfm.nav-back') }}</span>
                    </a>
                </div>
            </nav>
            <div class="visible-xs" id="current_dir"
                 style="padding: 5px 15px;background-color: #f8f8f8;color: #5e5e5e;"></div>

            <div id="alerts"></div>

            <div id="content"></div>
        </div>
        <ul id="fab">
            <li>
                <a href="#"></a>
                <ul class="hide">
                    <li>
                        <a href="#" id="add-folder" data-mfb-label="{{ trans('laravel-filemanager::lfm.nav-new') }}">
                            <i class="fa fa-folder"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" id="upload" data-mfb-label="{{ trans('laravel-filemanager::lfm.nav-upload') }}">
                            <i class="fa fa-upload"></i>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <a href="#" id="use" data-mfb-label="{{ __('app.use') }}" class="mfb-component__button--child">
            <i class="fa fa-download mfb-component__child-icon"></i>
        </a>
    </div>
</div>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">{{ trans('laravel-filemanager::lfm.title-upload') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('unisharp.lfm.upload') }}" role='form' id='uploadForm' name='uploadForm'
                      method='post' enctype='multipart/form-data' class="dropzone">
                    <div class="form-group" id="attachment">
                        <div class="controls text-center">
                            <div class="input-group" style="width: 100%">
                                <span class="btn btn-primary"
                                      id="upload-button">{{ trans('laravel-filemanager::lfm.message-choose') }}</span>
                            </div>
                        </div>
                    </div>
                    <input type='hidden' name='working_dir' id='working_dir'>
                    <input type='hidden' name='type' id='type' value='{{ request("type") }}'>
                    <input type='hidden' name='_token' value='{{csrf_token()}}'>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">{{ trans('laravel-filemanager::lfm.btn-close') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="folderModal" tabindex="-1" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('laravel-filemanager::lfm.message-name') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                </button>
            </div>
            <div class="modal-body">
                <form class="">
                    <div class="form-group" id="attachment">
                        <input class="form-control" autocomplete="off" type="text">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" type="button"
                        class="btn btn-default">{{ trans('laravel-filemanager::lfm.btn-close') }}</button>
                <button data-dismiss="modal" type="button"
                        class="btn btn-primary">{{ trans('laravel-filemanager::lfm.nav-new') }}</button>
            </div>
        </div>
    </div>
</div>

<div id="lfm-loader">
    <img src="{{asset('vendor/laravel-filemanager/img/loader.svg')}}">
</div>

<script src="{{ asset('vendor/admin-panel/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('vendor/admin-panel/js/popper.min.js') }}"></script>
<script src="{{ asset('vendor/admin-panel/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('vendor/laravel-filemanager/js/cropper.min.js') }}"></script>
<script src="{{ asset('vendor/laravel-filemanager/js/jquery.form.min.js') }}"></script>
<script src="{{ asset('vendor/laravel-filemanager/js/dropzone.min.js') }}"></script>
<script>
    var route_prefix = "{{ url('/') }}";
    var lfm_route = "{{ url(config('lfm.url_prefix', config('lfm.prefix'))) }}";
    var lang = {!! json_encode(trans('laravel-filemanager::lfm')) !!};
</script>
<script src="{{ asset('vendor/laravel-filemanager/js/script.js') }}"></script>
<script>
    $.fn.fab = function () {
        var menu = this;
        menu.addClass('mfb-component--br mfb-zoomin').attr('data-mfb-toggle', 'hover');
        var wrapper = menu.children('li');
        wrapper.addClass('mfb-component__wrap');
        var parent_button = wrapper.children('a');
        parent_button.addClass('mfb-component__button--main')
            .append($('<i>').addClass('mfb-component__main-icon--resting fa fa-plus'))
            .append($('<i>').addClass('mfb-component__main-icon--active fa fa-times'));
        var children_list = wrapper.children('ul');
        children_list.find('a').addClass('mfb-component__button--child');
        children_list.find('i').addClass('mfb-component__child-icon');
        children_list.addClass('mfb-component__list').removeClass('hide');
    };
    $('#fab').fab({
        buttons: [
            {
                icon: 'fa fa-folder',
                label: "{{ trans('laravel-filemanager::lfm.nav-new') }}",
                attrs: {id: 'add-folder'}
            },
            {
                icon: 'fa fa-upload',
                label: "{{ trans('laravel-filemanager::lfm.nav-upload') }}",
                attrs: {id: 'upload'}
            }
        ]
    });

    Dropzone.options.uploadForm = {
        paramName: "upload[]", // The name that will be used to transfer the file
        uploadMultiple: false,
        parallelUploads: 5,
        clickable: '#upload-button',
        dictDefaultMessage: "{{ __('app.dropdown') }}",
        init: function () {
            var _this = this; // For the closure
            this.on('success', function (file, response) {
                if (response == 'OK') {
                    refreshFoldersAndItems('OK');
                } else {
                    this.defaultOptions.error(file, response.join('\n'));
                }
            });
        },
        acceptedFiles: "{{ lcfirst(str_singular(request('type') ?: '')) == 'image' ? implode(',', config('lfm.valid_image_mimetypes')) : implode(',', config('lfm.valid_file_mimetypes')) }}",
        maxFilesize: ({{ lcfirst(str_singular(request('type') ?: '')) == 'image' ? config('lfm.max_image_size') : config('lfm.max_file_size') }} / 1000)
    }
</script>
</body>
</html>
