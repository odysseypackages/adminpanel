@section('js')
    <script src="/vendor/admin-panel/js/jquery-3.2.1.min.js"></script>
    <script src="/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="/vendor/admin-panel/js/popper.min.js"></script>
    <script src="/vendor/admin-panel/js/bootstrap.min.js"></script>
    <script src="/vendor/admin-panel/js/plugins/jquery.dataTables.min.js"></script>
    <script src="/vendor/admin-panel/js/plugins/dataTables.bootstrap.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="/vendor/admin-panel/js/main.js"></script>
    @stack('datatables-js')
@show
