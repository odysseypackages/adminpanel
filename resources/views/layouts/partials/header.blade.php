@section('header')
    <header class="app-header">
        <a class="app-header__logo" href="/">
            <span class="logo-big"><img class="img-fluid" src="/images/logo_odyssey.png"/> Odyssey<b>CMS</b></span>
            <span class="logo-mini"><img class="img-fluid" src="/images/logo_odyssey.png"/></span>
        </a>
        <span class="app-sidebar__toggle fa fa-bars" data-toggle="sidebar" aria-label="Hide Sidebar"></span>
        <ul class="app-nav">
            <li class="dropdown">
                <a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu" aria-expanded="false"><i class="fa fa-user fa-lg"></i></a>
                <ul class="dropdown-menu settings-menu dropdown-menu-right" x-placement="bottom-end">
                    <li><a class="dropdown-item" href=""><i class="fa fa-user fa-lg"></i>{{__('app.profile')}}</a></li>
                    <li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            @csrf()
                            <button type="submit" class="dropdown-item"><i class="fa fa-sign-out-alt fa-lg"></i>{{__('app.logout')}}</button>
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </header>
@show
