@extends('layouts.app')

@section('page-title')
    {{__('app.config.title')}}
@endsection

@section('title')
    {{__('app.config.title')}}
@endsection

@section('subtitle')
    {{__('app.config.subtitle')}}
@endsection

@section('breadcrumb')
    {!! Breadcrumb::make([__('app.config.title')]) !!}
@endsection

@section('page-content')
    <form method="POST" action="{{route('config.update')}}">
        @csrf()
        <button type="submit" class="btn btn-success">{{__('app.save')}}</button>
    </form>
@endsection
