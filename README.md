# AdminPanel

Package for fast and easy admin panels creation!

## Init
Add 

`Schema::defaultStringLength(191);`

In your _Providers/AppServiceProvider.php_

And use command

`php artisan admin:start --install`

and in your _Http/Kernel.php_ register routes middlewares

```
    protected $routeMiddleware = [
        ...
        'role' => \Odysseycrew\AdminPanel\Middlewares\RoleMiddleware::class,
        'permission' => \Odysseycrew\AdminPanel\Middlewares\PermissionMiddleware::class,
        'role_or_permission' => \Odysseycrew\AdminPanel\Middlewares\RoleOrPermissionMiddleware::class,
    ];
```

**Warning!**

Using this command can break your routes and views, so use it only on project Init!

## Usage

### Menu

Class `Odysseycrew\AdminPanel\Menu` helps you with building your app menu.
By default, menu instance used in app is created in `App\Http\Controllers\Controller`.

**Usage:**

```
        $menu = Menu::make(
            ['label', 'route_name', 'icon_css_class'],
            ['parent_label', 'parent_route_name', 'parent_icon_css_class',
                ['child_label', 'child_route_name', 'child_icon_css_class'],
                ...
            ],
            ...
        );
```

Menu element _route_name_ and _icon_css_class_ can be empty.

### Breadcrumb

**Usage:**

In view , just use:

`{!! Breadcrumb::make(['label','route_name'],['label']) !!}`

Elements without _route_name_ will not be clickable.

### LFM tag

In blade views:

`@lfm('name','buttonLabel',['imageOneUrl','imageTwoUrl',...])`

### DeleteButton tag

In blade views:

`@deleteButton('route')`