(function( $ ){

  $.fn.filemanager = function(type, options) {
    type = type || 'file';

    this.on('click', function(e) {
      var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
      localStorage.setItem('target_input', $(this).data('input'));
      localStorage.setItem('target_preview', $(this).data('preview'));
      window.open(route_prefix + '?type=' + type, 'FileManager', 'width=900,height=600');
      window.SetUrl = function (url, file_path) {
          //set the value of the desired input to image url
          var target_input = $('#' + localStorage.getItem('target_input'));
          val = target_input.val();
          if(val.search(file_path) == -1){
              target_input.val(val+' '+file_path).trigger('change');
              var target_preview = '<div class="holder"><img src="'+url+'" data-target="'+localStorage.getItem('target_input')+'"><i class="fa fa-close"></i></div>';
              target_input.parent().siblings('.gallery').append(target_preview);
          }
      };
      return false;
    });
  }

  $('body').on('click','.gallery .holder',function(){
      let img = $(this).find('img');
      let target = img.attr('data-target');
      let image = img.attr('src');
      let value  = $('#'+target).val();
      let newValue = value.replace(image,'');
      $('#'+target).val(newValue);
      $(this).remove();
  });

})(jQuery);
