<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

class AdminPanelSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'superadmin']);
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'user']);

        $user = User::create(['name' => 'Superadmin', 'email' => 'superadmin@example.com', 'password' => Hash::make('secret')]);
        $user->assignRole('superadmin');
    }
}
