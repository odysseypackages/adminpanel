<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
        'name', 'label', 'value', 'category'
    ];

    static public function getCategories()
    {
        return self::distinct('category')->pluck('category');
    }

    static public function SystemNameFields($name)
    {
        return self::where('category', $name)->pluck('name');
    }
}
