<?php

namespace Odysseycrew\AdminPanel;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Odysseycrew\AdminPanel\Router as Router;
use Illuminate\Support\Facades\Blade;

class AdminPanelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../resources/views/' => resource_path() . '/views',
            __DIR__ . '/../resources/lang/' => resource_path() . '/lang',
            __DIR__ . '/../public_share/' => public_path(),
            __DIR__ . '/../config/' => config_path()
        ], 'adminPanel');
        $this->publishes(
            [
                __DIR__ . '/../src/Helpers/' => app_path() . '/Helpers',
                __DIR__ . '/../src/Controllers/' => app_path() . '/Http/Controllers/',
                __DIR__ . '/../src/seeds/' => database_path().'/seeds',
                __DIR__ . '/../src/Models/' => app_path(),
            ], 'adminPanelInstall');
        if ($this->app->runningInConsole()) {
            $this->commands([
                AdminStartCommand::class,
            ]);
        }
        $this->loadViewsFrom(__DIR__ . '/packages/Menu/views', 'menu');
        $this->loadViewsFrom(__DIR__ . '/packages/FileManager/views', 'lfm');
        $this->loadViewsFrom(__DIR__ . '/packages/deleteButton/views', 'deleteButton');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        Blade::directive('lfm', function ($expression) {
            $expression = explode(',', $expression);
            $id = $expression[0];
            if (key_exists(1, $expression)) {
                $name = $expression[1];
            } else {
                $name = "'app.lfm.choose'";
            }
            if (key_exists(2, $expression)) {
                $files = $expression[2];
                return "<?php echo FMS::draw($id, $name, $files); ?>";
            } else {
                return "<?php echo FMS::draw($id, $name); ?>";
            }
        });

        Blade::directive('deleteButton', function ($route) {
            return "<?php echo Odysseycrew\\AdminPanel\\DeleteButton::draw($route); ?>";
        });
    }

}
