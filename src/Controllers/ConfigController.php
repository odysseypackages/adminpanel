<?php

namespace App\Http\Controllers;

use App\Data;
use Illuminate\Http\Request;
use Illuminate\Config;
use Illuminate\Support\Facades\Artisan;

class ConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        return response()->view('config.edit');
    }

    public function update(Request $request)
    {
        alert()->success(__('app.config.alert.success'));
        return redirect()->route('config.edit');
    }
}
