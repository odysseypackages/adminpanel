<?php

namespace App\Helpers;

use Odysseycrew\AdminPanel\Menu;

class MenuHelper
{
    public static function build(){
        return Menu::make(
            [__("app.menu.start"), 'dashboard', 'fa-home'],
            [__("app.menu.config"), 'config.edit', 'fa-cog']//new_route
        )->output();
    }
}
