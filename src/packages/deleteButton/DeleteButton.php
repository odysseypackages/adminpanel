<?php

namespace Odysseycrew\AdminPanel;

class DeleteButton
{
    public static function draw($route)
    {
        return view('deleteButton::deleteButton', ['route' => $route]);
    }
}
