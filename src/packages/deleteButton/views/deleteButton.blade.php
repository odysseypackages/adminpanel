
    <span class="btn btn-danger" id="btn-delete" data-toggle="modal" data-target="#modal-delete">
        <i class="fa fa-trash"></i>{{_('Usuń')}}
    </span>

@section('modals')
    @parent
    <div class="modal fade in" tabindex="-1" role="dialog" id="modal-delete" aria-hidden="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h5 class="modal-title">{{_('Usuń')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{$route}}" id="form-delete" class="form-horizontal" method="post">
                    <div class="modal-body">
                        <div class="content">
                            <div class="form-group">
                                <p>{{_('Czy na pewno chcesz usunąć ten wpis?')}}</p>
                            </div>
                        </div>
                    </div>
                    @method('DELETE')
                    @csrf
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger" id="btn-delete">{{_('Tak')}}</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">{{_('Nie')}}</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection

