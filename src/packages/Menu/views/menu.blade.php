<ul class="app-menu">
    @foreach($elements as $element)
        @include('menu::item',['element' => $element])
    @endforeach
</ul>
