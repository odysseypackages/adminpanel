<?php

namespace Odysseycrew\AdminPanel;

class FileManagerService
{
    public static function draw($id, $name, $files = []){
        return view('lfm::lfm',['id' => $id,'name' => $name,'files' => $files]);
    }
}
