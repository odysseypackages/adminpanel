<div class="input-group">
   <span class="input-group-btn">
     <button id="lfm-{{$id}}" data-input="{{$id}}" data-preview="holder" class="btn btn-secondary">
       <i class="fa fa-image"></i> {{__($name)}}
     </button>
   </span>
    <input id="{{$id}}" class="form-control" type="hidden" name="{{$id}}">
</div>
<div class="gallery">
    @if(!empty($files))
        @foreach($files as $file)
            <div class="holder"><img src="{{$file}}" data-target="{{$id}}"><i class="fa fa-close"></i></div>
        @endforeach
    @endif
</div>

@section('js')
    @parent
    <script>
        $('#lfm-{{$id}}').filemanager('image');
    </script>
@endsection
