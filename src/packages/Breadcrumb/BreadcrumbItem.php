<?php

namespace Odysseycrew\AdminPanel;


class BreadcrumbItem
{
    protected $name;

    protected $route;

    public function __construct($name, $route)
    {
        $this->name = $name;
        $this->route = $route;
    }

    public function output(){
        if($this->route){
            return '<li class="breadcrumb-item"><a href="'.$this->route.'">'.$this->name.'</a></li>';
        }else{
            return '<li class="breadcrumb-item"><span>'.$this->name.'</span></li>';
        }
    }
}
