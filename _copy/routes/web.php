Route::middleware(['auth'])->group(function () {
    Route::get('/', 'DashboardController@index')->name('dashboard')->middleware('role:admin');
    Route::get('/config', 'ConfigController@edit')->name('config.edit')->middleware('role:admin');
    Route::post('/config', 'ConfigController@update')->name('config.update')->middleware('role:admin');
});

